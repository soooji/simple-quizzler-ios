//
//  ViewController.swift
//  Quizzler
//
//  Created by Angela Yu on 25/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Place your instance variables here
    let allQuestions = QuestionBank() // state
    var questionNumber : Int = 0 // state
    var pickedAnswer : Bool = false // state
    var score : Int = 0
    //Connections
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    //First did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
    }
    //Is answered
    @IBAction func answerPressed(_ sender: AnyObject) {
        if sender.tag == 1 {
            pickedAnswer = true
        } else if sender.tag == 2{
            pickedAnswer = false
        }
        if questionNumber < allQuestions.list.count-1 {
            checkAnswer(allQuestions.list[questionNumber])
            nextQuestion()
        } else {
            let alert = UIAlertController(title: "آفرین!", message: "شما به همه سؤالات پاسخ دادید، میخواین دوباره امتحان کنین؟", preferredStyle: .alert)
            
            let restartAction = UIAlertAction(title:"شروع", style: .default, handler: {
                (UIAlertAction) in
                self.startOver()
            })
            
            alert.addAction(restartAction)
            
            present(alert,animated: true,completion: nil)
        }
    }
    
    
    func updateUI() {
        questionLabel.text = allQuestions.list[questionNumber].questionText
        progressLabel.text = "\(questionNumber+1) / \(allQuestions.list.count)"
        scoreLabel.text = "امتیاز: \(score) / \(allQuestions.totalScore)"
        progressBar.frame.size.width = (view.frame.size.width / 13) * CGFloat(questionNumber+1)
    }
    

    func nextQuestion() {
        questionNumber = questionNumber + 1;
        updateUI()
    }
    
    
    func checkAnswer(_ question : Question) {
        if question.answer == pickedAnswer {
            ProgressHUD.showSuccess("درسته!")
            score = score + allQuestions.list[questionNumber].score
        } else {
            ProgressHUD.showError("جوابتون اشتباهه!")
        }
    }
    
    
    func startOver() {
        questionNumber = 0
        pickedAnswer = false
        score = 0
        updateUI()
    }
    

    
}
