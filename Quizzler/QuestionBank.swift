//
//  QuestionBank.swift
//  Quizzler
//
//  Created by Sajad Beheshti on 12/14/18.
//  Copyright © 2018 London App Brewery. All rights reserved.
//

import Foundation

class QuestionBank {
    
    var list = [Question]()
    var totalScore = 0
    init() {
        list.append(Question(text: "ولنتاین در عربستان ممنوعه؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "خون حلزون سبزه؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "تقریبا یک چهارم از استخوان های انسان در پا هستند؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "مساحت کل دو ریه انسان تقریبا 70 متر مربع است؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "در غرب ویرجینیا، ایالات متحده آمریکا، اگر شما به طور تصادفی به یک حیوان با ماشین خود ضربه زدید، شما آزاد هستید که آن را به خانه ببرید؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "در لندن، انگلستان، اگر در خانه مجلس نمایندگان بمیرید، به لحاظ فنی حق دارید به مراسم خاکسپاری دولتی بپردازید، زیرا این ساختمان یک مکان مقدس است؟", correctAnswer: false,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "در پرتغال، خوردن چیزی در اقیانوس غیر قانونی است؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "شما می توانید یک گاو را به پایین پله ها هدایت کنید اما نه به سمت بالای پله ها؟", correctAnswer: false,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "گوگل در ابتدا\"Backrub\"نامگذاری شد؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "نام مادر Buzz Aldrin's \"ماه\" بود؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "بلندترین صدای تولید شده توسط حیوان ها متعلق به فیل آفریقایی است با ۱۸۸ دسیبل؟", correctAnswer: false,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "هیچ قطعه ای از کاغذ خشک خرد شده را نمی توان بیش از ۷ بار نصف کرد؟", correctAnswer: false,scoreNumber: 3))
        addToTotalScore(3)
        
        list.append(Question(text: "شکلات بر قلب و سیستم عصبی سگ تاثیر می گذارد و چند انس آن برای کشتن سگ کوچک کافی است؟", correctAnswer: true,scoreNumber: 3))
        addToTotalScore(3)
    }
    
    func addToTotalScore(_ score : Int) {
        totalScore = totalScore + score
    }
}
