//
//  Question.swift
//  Quizzler
//
//  Created by NIC on 12/14/18.
//  Copyright © 2018 London App Brewery. All rights reserved.
//

import Foundation

class Question {
    
    let questionText : String
    let answer : Bool
    let score : Int
    
    init(text: String,correctAnswer: Bool,scoreNumber: Int) {
        questionText = text
        answer = correctAnswer
        score = scoreNumber
    }
}

